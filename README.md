# Github Profile Seaching
You can search Gith profiles through this application. It is a sigle page application(SPA). The application has been created for flawless Github profile search. There has been used Anguular 2 and Github search API. You can seach via username and name & then you can go single profile.

## Screenshots

![ScreenShot](/screenshots/screenshots1.PNG)

![ScreenShot](/screenshots/screenshot2.PNG)

## Installations
```
git clone https://gitlab.com/saimunhossain/AngularApp-Github-Profile-Searching
cd AngularApp-Github-Profile-Searching
npm install
npm serve

```

## Query

If you have any question feel free to ask me. My email has given on the top. Enjoy coding!